import * as core from "@actions/core";
import * as exec from "@actions/exec";
import * as github from "@actions/github";
import * as tc from "@actions/tool-cache";
import * as assert from "assert";
import * as fs from "fs";
import * as path from "path";
import { runFission } from "./utils";

const ASSET_NAME = "fission-cli-ubuntu-20.04-x86_64";

export const getFissionCLI = async () => {
  let release;
  if (process.env.GITLAB_CI) {
    release = {tag_name:"2.17.2"};
  } else {
    const githubToken = core.getInput("token");
    const octokit = github.getOctokit(githubToken);

    // Grab the latest release from github
    const { data } = await octokit.rest.repos.getLatestRelease({
      owner: "fission-suite",
      repo: "fission",
    });
    release = data;
  }

  core.info(`Using fission CLI version: ${release.tag_name}`);

  // Check the tool cache for the fission CLI.
  let toolPath: string;
  toolPath = tc.find("fission-cli", release.tag_name);
  if (!toolPath) {
    // Get the URL for the actual CLI version
    let downloadPath = "";
    if (process.env.GITLAB_CI) {
      downloadPath = await tc.downloadTool('https://github.com/fission-suite/fission/releases/download/2.17.2/fission-cli-ubuntu-20.04-x86_64');
    } else {
      const asset = release.assets.find((a) => a.name == ASSET_NAME);
      if (asset) {
        downloadPath = await tc.downloadTool(asset.browser_download_url);
      } else {
        core.info("Unable to find release download.");
      }
    }

    if (downloadPath) {
      await exec.exec("chmod", ["+x", downloadPath]);
      await exec.exec("ls", ["-al", downloadPath]);
      toolPath = await tc.cacheFile(
        downloadPath,
        "fission",
        "fission-cli",
        release.tag_name
      );
      await exec.exec("ls", ["-al", toolPath]);
    }
  }

  core.addPath(toolPath);
};

export const importKey = async (key: string) => {
  const tempDir = process.env["RUNNER_TEMP"] || "";
  assert.ok(tempDir, "Expected RUNNER_TEMP to be defined");

  const keyFile = path.join(tempDir, "machine_id.ed25519");

  let buff = Buffer.from(key, "base64");
  fs.writeFileSync(keyFile, buff);

  await runFission(["setup", "--with-key", keyFile]);
};
